# -*- coding: utf-8 -*-
{
    'name':'Mobile POS',
    'summary':'Login and open internal messages in mobile app, requiere Mobile Pos installed',
    'category': 'mail',
    'version':'1.0',
    'author':'Daniel Santibáñez Polanco',
    'website': 'http://globalresponse.cl',
    'data': [
        'views/res_users.xml',
        'views/assets.xml',
    ],
    'depends': [
        'base',
        'web',
        'mail'
    ],
    'external_dependencies': {
        'python': [
            'io',
            'qrcode',
            'base64'
        ]
    },
    'qweb': [
        "static/src/xml/base.xml",
    ],
    'application': True,
    'description': '''Enable create internal deeplink and qr login for use in mobile app descargar en https://play.google.com/store/apps/details?id=com.mobile_pos'''
}
