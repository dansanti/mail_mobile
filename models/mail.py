import re
from odoo import _, api, models
import logging
_logger = logging.getLogger(__name__)


class MailTemplate(models.Model):
    _inherit = 'mail.template'

    @api.multi
    def generate_email(self, res_ids, fields=None):
        obj = self.with_context(mail_mobile=True)
        return super(MailTemplate, obj).generate_email(res_ids, fields=fields)

    @api.model
    def _append_handler_body(self, body):
        target = '(<a href="[a-zA-Z 0-9 \/\.\:\;\=\_\?]*)(message_id=[0-9]*)"'
        reemplazo = r'<a href="mobileapp://backend/\2"  deeplink="true" style="padding: 8px 12px; font-size: 12px; color: #FFFFFF; text-decoration: none !important; font-weight: 400; background-color: #875A7B; border: 0px solid #875A7B; border-radius:3px" >Ver en Mobile App</a> \1\2"'
        body = re.sub(target, reemplazo, body)
        target = '(<a href="[a-zA-Z 0-9 \/\.\:\;\=\_\?]*)(model=[0-9a-zA-Z.\;\_\=]*)&amp;(res_id=[0-9]*)"'
        reemplazo = r'<a href="mobileapp://backend/\2/\3" deeplink="true" style="padding: 8px 12px; font-size: 12px; color: #FFFFFF; text-decoration: none !important; font-weight: 400; background-color: #875A7B; border: 0px solid #875A7B; border-radius:3px" >Ver en Mobile App</a> \1\2&amp;\3"'
        return re.sub(target, reemplazo, body)

    @api.model
    def render_template(self, template_txt, model, res_ids,
                        post_process=False):
        res = super(MailTemplate, self).render_template(
            template_txt, model, res_ids, post_process=post_process,
        )
        if post_process and self.env.context.get('mail_mobile'):
            if isinstance(res, str):
                res = self._append_handler_body(res)
            else:
                for res_id, body in res.items():
                    res[res_id] = self._append_handler_body(body)
        return res
